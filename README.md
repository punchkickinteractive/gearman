# Punchkick Gearman Worker

A Gearman worker setup that works with our deployment methods.

## Why?

Stopping and starting a Gearman worker during deployment can cause jobs to be killed right in the middle. We need a way to stop the running workers, but wait for them to complete the current job. We also want to be able to run jobs in bursts in case they take up a lot of bandwidth or processing power. This lib allows you to easily write a worker script that can be softly killed with a SIGHUP. You can also terminate the script immediately using a SIGTERM. You can set how many jobs to run in a burst and how long to wait between each burst.

## Adding to your project

Until this package is made public on packagist, you'll need to add the repository to your composer.json and require the package.

~~~json
{
    "repositories": [
        {
            "type": "vcs",
            "url": "https://bitbucket.org/punchkickinteractive/gearman.git"
        }
    ],
    "require": {
        "punchkick/gearman": "dev-master"
    }
}
~~~

## Jobs

Creating a job is as simple as extending the AbstractJob class and overwriting the addFunctions method. Here is an example:

~~~php
<?php

class EmailJob extends Punchkick\Gearman\AbstractJob
{
    public function addFunctions()
    {
        $this->worker->addFunction('some_function', function (GearmanJob $job) {
            echo 'some_function running' . PHP_EOL;
            echo $job->workload() . PHP_EOL;
        });
    }
}
~~~

## Your Script

Your Gearman worker script simply needs to bootstrap the Worker class and tell it to get started. Here is an example:

~~~php
<?php
require 'vendor/autoload.php';

// Create an instance of GearmanWorker and add your server(s)
$gearmanWorker = new GearmanWorker();
$gearmanWorker->addServer();

// Create an instance of the Worker class and configure it
$worker = new Punchkick\Gearman\Worker($gearmanWorker);

/**
 * Optionally set bursting options
 */
$worker->setJobsPerBurst(100); // how many jobs to run per burst
$worker->setSleepSecs(5); // how long to sleep between each burst


// Add your custom job(s)
$emailJob = new EmailJob();
$worker->addTask($emailJob);

/**
 * Optionally add logging
 * Any logger will do, as long as it implements the PSR-3 logging interface
 * See https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-3-logger-interface.md
 */
$logger = new Monolog\Logger('Gearman'); 
$log->pushHandler(new StreamHandler('logs/app.log', Monolog\Logger::INFO));
$worker->addLogger($logger);

// Get started
$worker->doWork();

~~~