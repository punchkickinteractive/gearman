<?php

namespace Punchkick\Gearman;

use GearmanWorker;

/**
 * Interface JobInterface
 * @package Punchkick\Gearman
 */
interface JobInterface
{
    /**
     * @param GearmanWorker $worker
     * @return void
     */
    public function setWorker(GearmanWorker $worker);

    /**
     * @return GearmanWorker
     */
    public function getWorker();

    /**
     * @return bool
     */
    public function addFunctions();
}
