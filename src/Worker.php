<?php

namespace Punchkick\Gearman;

use Psr\Log\LoggerInterface;
use GearmanWorker;

/**
 * Class Worker
 * @package Punchkick\Gearman
 */
class Worker
{
    /**
     * @var int
     */
    protected $jobsPerBurst;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var int
     */
    protected $sleepSecs;

    /**
     * @var array
     */
    protected $tasks;

    /**
     * @var bool
     */
    protected $terminate;

    /**
     * @var GearmanWorker
     */
    protected $worker;

    /**
     * @param GearmanWorker $worker
     * @param LoggerInterface|null $logger
     */
    public function __construct(GearmanWorker $worker, LoggerInterface $logger = null)
    {
        $this->worker = $worker;

        if ($logger) {
            $this->logger = $logger;
        }

        $this->terminate = false;

        // Default values for $jobsPerBurst and $sleepSecs (assumes no sleeping)
        $this->jobsPerBurst = 1;
        $this->sleepSecs = 0;
    }

    /**
     * @return void
     */
    public function doWork()
    {
        declare(ticks = 1);

        // allows us to restart process without losing emails
        $that = $this;
        pcntl_signal(SIGHUP, function () use (&$that) {
            $this->terminate = true;
        });

        // kills the stript on signal terminate
        pcntl_signal(SIGTERM, function () {
            exit(0);
        });

        $this->log('info', 'gearman worker started');

        $this->worker->setOptions(GEARMAN_WORKER_NON_BLOCKING);

        if ($this->tasks) {
            /** @var AbstractJob $task */
            foreach ($this->tasks as $task) {
                $task->setWorker($this->worker);
                $task->addFunctions();
            }
        }

        $this->workLoop();
    }

    /**
     * @param AbstractJob $task
     */
    public function addTask(AbstractJob $task)
    {
        $this->tasks[] = $task;
    }

    /**
     * @return GearmanWorker
     */
    public function getWorker()
    {
        return $this->worker;
    }

    /**
     * @param GearmanWorker $worker
     */
    public function setWorker($worker)
    {
        $this->worker = $worker;
    }

    /**
     * @return LoggerInterface
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * @param LoggerInterface $logger
     */
    public function setLogger($logger)
    {
        $this->logger = $logger;
    }

    /**
     * @return int
     */
    public function getSleepSecs()
    {
        return $this->sleepSecs;
    }

    /**
     * @param int $sleepSecs
     */
    public function setSleepSecs($sleepSecs)
    {
        $this->sleepSecs = $sleepSecs;
    }

    /**
     * @return int
     */
    public function getJobsPerBurst()
    {
        return $this->jobsPerBurst;
    }

    /**
     * @param int $jobsPerBurst
     */
    public function setJobsPerBurst($jobsPerBurst)
    {
        $this->jobsPerBurst = $jobsPerBurst;
    }

    /**
     * @return void
     */
    protected function workLoop()
    {
        $runningCount = $this->jobsPerBurst + 1;

        while (
            $this->terminate === false
            and (
                $this->worker->work()
                or $this->worker->returnCode() === GEARMAN_IO_WAIT // 1
                or $this->worker->returnCode() === GEARMAN_NO_JOBS // 35
            )
        ) {
            if ($this->worker->returnCode() === GEARMAN_SUCCESS) { // 0
                continue;
            }

            if (!--$runningCount) {
                $runningCount = $this->jobsPerBurst;
                sleep($this->sleepSecs);
            }

            $this->worker->wait();
        }

        posix_kill(getmypid(), SIGTERM);
    }

    /**
     * @param $level
     * @param $message
     * @param array $context
     */
    protected function log($level, $message, array $context = array())
    {
        if ($this->logger and method_exists($this->logger, $level)) {
            $this->logger->$level($message, $context);
        }
    }

}
