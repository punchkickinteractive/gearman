<?php

namespace Punchkick\Gearman;

use GearmanWorker;

/**
 * Class AbstractJob
 * @package Punchkick\Gearman
 */
abstract class AbstractJob implements JobInterface
{
    /**
     * @var GearmanWorker
     */
    protected $worker;

    /**
     * @return GearmanWorker
     */
    public function getWorker()
    {
        return $this->worker;
    }

    /**
     * @param GearmanWorker $worker
     */
    public function setWorker(GearmanWorker $worker)
    {
        $this->worker = $worker;
    }

}